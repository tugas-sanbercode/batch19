// Soal 1

function arrayToObject(arr) {
    var obj={};
    for(var i=0; i<arr.length; i++){
        var now = new Date()
        var thisYear = now.getFullYear() // 2020 (tahun sekarang)
        var fullname = arr[i][0] + ' ' + arr[i][1];
        var age = arr[i][3] > thisYear || arr[i][3]==null ? 'Invalid Birth Year' : thisYear - arr[i][3];
        obj={
            firstName:arr[i][0],
            lastName:arr[i][1],
            gender:arr[i][2],
            age:age,
        }
        
        console.log(i+1 + '. ' + fullname +': ');
        console.log(obj)
    }
}

console.log("\r\nSoal 1\r\n==============")

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
 
// Error case 
arrayToObject([]) // ""


// Soal 2

function shoppingTime(memberId, money) {

    var stok={
        "Sepatu Stacattu": 1500000,
        "Baju Zoro": 500000,
        "Baju H&N": 250000,
        "Sweater Uniklooh": 175000,
        "Casing Handphone": 50000,
    }

    if(!memberId){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }

    if(money<50000){
        return "Mohon maaf, uang tidak cukup";
    }

    var saldo=money;
    var listBuy=[];
    Object.keys(stok).forEach( (key) => {
        var value = stok[key];

        if( saldo >= value){
            saldo = saldo-value;
            listBuy.push(key);
        }
    })

    
    var obj={
        memberId: memberId,
        money: money,
        listPurchased: listBuy,
        changeMoney: saldo
    }

    return obj;
}
  
console.log("\r\nSoal 2\r\n==============")

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal 3

function naikAngkot(arrPenumpang) {

    var obj=[];
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];

    for(var j=0; j<arrPenumpang.length; j++){
    
        var penumpang = arrPenumpang[j][0];
        var naikDari = arrPenumpang[j][1];
        var tujuan = arrPenumpang[j][2];
        var bayar=0;

        var naik=0;
        for(var i=0; i<rute.length; i++){
            
            if(rute[i]==naikDari){
                naik=1;
            }
            if(rute[i]==tujuan){
                naik=0;
            }

            if(naik){
                bayar+=2000;
            }
        }

        obj[j]={
            penumpang,
            naikDari,
            tujuan,
            bayar
        }
    }

    return obj;
}

console.log("\r\nSoal 3\r\n==============")

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); //[]