var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

var i=0;

function mulaiBaca(time){
	readBooksPromise( time, books[i] )
		.then(function (time) {
            i++;
            if(books[i] != null){
                mulaiBaca(time);
            }
		})
		.catch(function (time) {
			
		});
}

mulaiBaca(5000);