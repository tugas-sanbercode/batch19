import React from 'react';
import YoutubeUI from './Tugas/Tugas12/App';
import {LoginScreen, RegisterScreen} from './Tugas/Tugas13/LoginScreen';
import AboutScreen from './Tugas/Tugas13/AboutScreen';
import ToDoApp from './Tugas/Tugas14/App';
import Navigation from './Tugas/Tugas15/index'
import TugasNavigation from './Tugas/TugasNavigation/index'
import Quiz3 from './Tugas/Quiz3/index'

export default function App() {
  return (
    // <YoutubeUI />
    // <LoginScreen />
    // <RegisterScreen />
    // <AboutScreen />
    // <ToDoApp />
    // <Navigation />
    // <TugasNavigation />
    <Quiz3 />
  );
};