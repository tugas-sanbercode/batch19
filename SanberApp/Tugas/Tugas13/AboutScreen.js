import React from 'react'
import {
    View, 
    Text, 
    Image, 
    TouchableOpacity, 
    KeyboardAvoidingView, 
    ScrollView,
    Platform
} from 'react-native'
import styles from './styles.js'
import { AntDesign, Entypo } from '@expo/vector-icons'; 

const AboutScreen = () => {

    return (
        <KeyboardAvoidingView 
            behavior={Platform.OS == 'ios' ? "padding" : "height"}
            style={styles.container}    
        >
            <View style={styles.topHeader}>
                <Text style={styles.appTitle}>SanberApp</Text>
            </View>
            <ScrollView>
                <View style={styles.containerView}>
                    <View style={styles.mainBody}>
                        <View style={{alignItems:'center'}}>
                            <Image style={styles.profileImage} resizeMode="contain" source={require('./images/tom.jpg')} />
                        </View>
                        <View>
                            <Text style={styles.profileName}>Deczen De Kristo</Text>
                        </View>
                        <View style={{marginTop:20}}>
                            <View style={styles.socialIcon}>
                                <Entypo name="facebook-with-circle" size={30} color="#3b5998" />
                                <Text style={styles.socialIconText}>/deczen</Text>
                            </View>
                            <View style={styles.socialIcon}>
                                <Entypo name="twitter-with-circle" size={30} color="#1DA1F2" />
                                <Text style={styles.socialIconText}>@deczen</Text>
                            </View>
                            <View style={styles.socialIcon}>
                                <Entypo name="instagram-with-circle" size={30} color="#bc2a8d" />
                                <Text style={styles.socialIconText}>@deczen</Text>
                            </View>
                        </View>
                        <View style={{alignItems:'flex-start', marginTop:20}}>
                            <TouchableOpacity>
                                <Text style={styles.linkText}>Portfolio</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
            <View style={styles.tabBar}>
                <TouchableOpacity style={styles.tabItem}>
                    <AntDesign name="home" size={24} color="black" />
                    <Text style={styles.tabTitle}>Home</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem}>
                    <AntDesign name="profile" size={24} color="black" />
                    <Text style={styles.tabTitle}>About</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem}>
                    <AntDesign name="logout" size={24} color="black" />
                    <Text style={styles.tabTitle}>Logout</Text>
                </TouchableOpacity>
            </View>
        </KeyboardAvoidingView>
    )
}

export default AboutScreen