import React from 'react'
import {
    View, 
    Text, 
    Image, 
    TextInput, 
    TouchableOpacity, 
    KeyboardAvoidingView, 
    ScrollView,
    Platform
} from 'react-native'
import styles from './styles.js'

const LoginScreen = () => {

    return (
        <KeyboardAvoidingView 
            behavior={Platform.OS == 'ios' ? "padding" : "height"}
            style={styles.container}    
        >
            <View style={styles.topHeader}>
                <Text style={styles.appTitle}>SanberApp</Text>
            </View>
            <ScrollView>
                <View style={styles.containerView}>
                    <View style={styles.mainBody}>
                        <View style={{alignItems:'center'}}>
                            <Image style={styles.logo} source={require('./images/logo.png')} />
                        </View>
                        <View>
                            <Text style={styles.screenTitle}>LOGIN</Text>
                        </View>
                        <View style={styles.formInput}>
                            <Text>Username/email</Text>
                            <TextInput style={styles.input} />
                        </View>
                        <View style={styles.formInput}>
                            <Text>Password</Text>
                            <TextInput style={styles.input} secureTextEntry={true} />
                        </View>
                        <View style={{alignItems:'center', marginTop: 20}}>
                            <TouchableOpacity style={styles.mainBtn}>
                                <Text style={styles.mainBtnText}>Login</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{alignItems:'center', marginTop:20}}>
                            <TouchableOpacity>
                                <Text style={styles.linkText}>Does not have account?</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </KeyboardAvoidingView>
    )
}

const RegisterScreen = () => {

    return (
        <KeyboardAvoidingView 
            behavior={Platform.OS == 'ios' ? "padding" : "height"}
            style={styles.container}    
        >
            <View style={styles.topHeader}>
                <Text style={styles.appTitle}>SanberApp</Text>
            </View>
            <ScrollView>
                <View style={styles.containerView}>
                    <View style={styles.mainBody}>
                        <View style={{alignItems:'center'}}>
                            <Image style={styles.logo} source={require('./images/logo.png')} />
                        </View>
                        <View>
                            <Text style={styles.screenTitle}>REGISTER</Text>
                        </View>
                        <View style={styles.formInput}>
                            <Text>Email</Text>
                            <TextInput style={styles.input} />
                        </View>
                        <View style={styles.formInput}>
                            <Text>Password</Text>
                            <TextInput style={styles.input} secureTextEntry={true} />
                        </View>
                        <View style={styles.formInput}>
                            <Text>Re-enter Password</Text>
                            <TextInput style={styles.input} secureTextEntry={true} />
                        </View>
                        <View style={{alignItems:'center', marginTop: 20}}>
                            <TouchableOpacity style={styles.mainBtn}>
                                <Text style={styles.mainBtnText}>Register</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{alignItems:'center', marginTop:20}}>
                            <TouchableOpacity>
                                <Text style={styles.linkText}>Already have an account?</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </KeyboardAvoidingView>
    )
}

export { LoginScreen, RegisterScreen }