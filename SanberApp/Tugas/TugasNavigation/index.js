import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Login from './LoginScreen';
import Skill from './SkillScreen';
import Project from './ProjectScreen';
import Add from './AddScreen';
import About from './AboutScreen';

const LoginStack = createStackNavigator();
const SkillStack = createStackNavigator();
const ProjectStack = createStackNavigator();
const AddStack = createStackNavigator();
const AboutStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const LoginStackScreen = () => (
  <LoginStack.Navigator screenOptions={{
    headerShown: false
  }}>
    <LoginStack.Screen name="Login" component={Login} />    
    <LoginStack.Screen name="Skill" component={DrawerFunc} />

  </LoginStack.Navigator>
)

const SkillStackScreen = () => (
    <SkillStack.Navigator  screenOptions={{
        headerShown: false
    }}>
        <SkillStack.Screen name="Skill" component={TabsScreen} />
    </SkillStack.Navigator>
)

const ProjectStackScreen = () => (
    <ProjectStack.Navigator screenOptions={{
        headerShown: false
      }}>
        <ProjectStack.Screen name="Project" component={Project} />
    </ProjectStack.Navigator>
)

const AddStackScreen = () => (
    <AddStack.Navigator screenOptions={{
        headerShown: false
      }}>
        <AddStack.Screen name="Add" component={Add} />
    </AddStack.Navigator>
)

const AboutStackScreen = () => (
    <AboutStack.Navigator screenOptions={{
        headerShown: false
      }}>
        <AboutStack.Screen name="About" component={About} />
    </AboutStack.Navigator>
)


const TabsScreen = () => (  
    <Tabs.Navigator screenOptions={{
        headerShown: false
      }}>
        <Tabs.Screen name="Skill" component={Skill} />
        <Tabs.Screen name="Project" component={ProjectStackScreen} />
        <Tabs.Screen name="Add" component={AddStackScreen} />
    </Tabs.Navigator>
)

const DrawerFunc = () => (
    <Drawer.Navigator screenOptions={{
        headerShown: false
      }}>
      <Drawer.Screen name="Home" component={TabsScreen} />
      <Drawer.Screen name="About" component={AboutStackScreen} />
    </Drawer.Navigator>
)

export default () => (
  <NavigationContainer>
    <LoginStackScreen />
  </NavigationContainer>
)
