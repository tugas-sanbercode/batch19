import React from "react";
import { View, Text, StyleSheet, Button, Alert } from "react-native";

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center"
    },
    button: {
      paddingHorizontal: 20,
      paddingVertical: 10,
      marginVertical: 10,
      borderRadius: 5
    }
  });  

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

 const Login = ({ navigation }) => (
  <ScreenContainer>
    <Text>Login Screen</Text>
    <Button 
      title="Login" 
      onPress={() => 
        navigation.push('Skill', { name: "Skill Screen" })
      } 
    />
  </ScreenContainer>
);

export default Login;