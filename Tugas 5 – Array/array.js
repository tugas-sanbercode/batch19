// Soal 1

function range(startNum, finishNum){

    var array=[];

    if(startNum < finishNum){
        for(var x=startNum; x<=finishNum; x++){
            array.push(x);
        }
    }else if(startNum > finishNum){
        for(var x=startNum; x>=finishNum; x--){
            array.push(x);
        }
    }else{
        array=-1;
    }

    return array;
}

console.log("\r\nSoal 1\r\n==============")
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


// Soal 2

function rangeWithStep(startNum, finishNum, step) {

    var array=[];

    if(startNum < finishNum){
        for(var x=startNum; x<=finishNum; x=x+step){
            array.push(x);
        }
    }else if(startNum > finishNum){
        for(var x=startNum; x>=finishNum; x=x-step){
            array.push(x);
        }
    }

    return array;
}

console.log("\r\nSoal 2\r\n==============")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


// Soal 3

// Code di sini

function sum(startNum=0, finishNum, step) {

    if( step ){
        var array = rangeWithStep(startNum, finishNum, step);
    }else if(startNum && finishNum){
        var array = range(startNum, finishNum);
    }else{
        var array = [startNum];
    }

    // console.log(array);

    if(Array.isArray(array)){
        var sum = array.reduce(function(a, b){
            return a + b;
        }, 0);
    }else{
        sum = array;
    }

    return sum;
}


console.log("\r\nSoal 3\r\n==============")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


// Soal 4

function dataHandling(n){

    for(var x = 0; x < n.length; x++){
        
        console.log('Nomor ID: ' + n[x][0])
        console.log('Nama Lengkap: ' + n[x][1])
        console.log('TTL: ' + n[x][2] + ' ' + n[x][3])
        console.log('Hobi: ' + n[x][4])
        
        console.log();
    }
}

//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

console.log("\r\nSoal 4\r\n==============")
dataHandling(input)

// Soal 5

function balikKata(kata){
    var kataBaru='';
    for(var x=kata.length-1; x>=0; x--){
        kataBaru+=kata[x];
    }

    return kataBaru;
}

console.log("\r\nSoal 5\r\n==============")
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal 6

function dataHandling2(n){
    var new_n = n.slice();

    new_n[1] = new_n[1] + 'Elsharawy';
    new_n[2] = 'Provinsi ' + new_n[2];
    new_n.pop();
    new_n.push('Pria');
    new_n.push('SMA Internasional Metro');

    var nama = n[1];
    var date = new_n[3];
    var date_arr = date.split('/');
    var day = date_arr[0];
    var month = parseInt(date_arr[1]);
    var year = date_arr[2];
    var month_text = '';
    var date_new = date_arr.join('-');

    switch(month){
        case 1: month_text='Januari'; break;
        case 2: month_text='Februari'; break;
        case 3: month_text='Maret'; break;
        case 4: month_text='April'; break;
        case 5: month_text='Mei'; break;
        case 6: month_text='Juni'; break;
        case 7: month_text='Juli'; break;
        case 8: month_text='Agustus'; break;
        case 9: month_text='September'; break;
        case 10: month_text='Oktober'; break;
        case 11: month_text='November'; break;
        case 12: month_text='Desember'; break;
    }

    console.log(new_n);
    console.log(month_text);
    console.log(date_arr.sort( function(n,y){
        return y - n;
    }));
    console.log(date_new);
    console.log(nama);
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

console.log("\r\nSoal 6\r\n==============")
dataHandling2(input);