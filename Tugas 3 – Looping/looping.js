// Soal 1

var mode='maju';
var i=1;

console.log("\r\nSoal 1\r\n==============")
while (i<=21 && mode!='stop'){
    

    if(mode=='maju' && i>1){
        console.log( i + ' - ' + 'I love coding');
    }else if(mode=='maju' && i==1){
        console.log('LOOPING PERTAMA');
    }else if(mode=='mundur' && i <= 20){
        console.log( i + ' - ' + 'I will become a mobile developer');
    }else if(mode=='mundur' && i > 20){
        console.log('LOOPING KEDUA');
    }

    if(mode=='maju'){
        i++;
    }else if(mode=='mundur'){
        i--;
    }

    if(mode=='maju' && i>20){
        mode='mundur';
    }

    if(mode=='mundur' && i==1){
        mode='stop';
    }
}

// Soal 2

var text_to_show = '';

console.log("\r\nSoal 2\r\n==============")
for(var i=1; i<=20; i++){
	
	if(i%3==0 && i%2==1){
		text_to_show='I Love Coding';
	}else if(i%2==0){
		text_to_show='Berkualitas';
	}else if(i%2==1){
		text_to_show='Santai';
	}
	
	console.log(i.toString() + ' - ' + text_to_show);
}

// Soal 3

var line='';

console.log("\r\nSoal 3\r\n==============")
for( var i=0; i<4; i++){

    line='';
    for(var j=0; j<8; j++){
        line+='#';
    }

    console.log(line)
}

// Soal 4

var kress='';

console.log("\r\nSoal 4\r\n==============")
for(var i=0; i<7; i++){
	
	kress='#';
	
	for(var j=0; j<i; j++){
		
		kress=kress+'#';
	}
	
	console.log(kress);
}

// Soal 5

var line = '';

console.log("\r\nSoal 5\r\n==============")
for(var i=0; i<8; i++){

    line='';
    for(var j=0; j<8; j++){

        if(i%2==0){
            if(j%2==0){
                line+=' ';
            }else{
                line+='#';
            }
        }else{
            if(j%2==0){
                line+='#';
            }else{
                line+=' ';
            }
        }
    }

    console.log(line);
}