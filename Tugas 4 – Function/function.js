// Soal 1

function teriak(){

    return "Halo Sanbers!"
}
 
console.log("\r\nSoal 1\r\n==============")
console.log(teriak()) // "Halo Sanbers!" 

// Soal 2

function kalikan(num1, num2){

    return Number(num1) * Number(num2)
}
 
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log("\r\nSoal 2\r\n==============")
console.log(hasilKali) // 48


// Soal 3

function introduce(name, age, address, hobby){

    return "Nama saya "+ name +", umur saya "+ age +" tahun, alamat saya di "+ address +", dan saya punya hobby yaitu "+ hobby +"!";
}
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log("\r\nSoal 3\r\n==============")
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 