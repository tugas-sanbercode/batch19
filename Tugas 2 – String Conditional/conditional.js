// Soal If-Else

var nama = "John"
var peran = ""

console.log("\r\nSoal If-Else\r\n==============")

// Output untuk Input nama = '' dan peran = ''
if(nama==='' && peran===''){
    console.log("Nama harus diisi!")
}
 
//Output untuk Input nama = 'John' dan peran = ''
else if(nama==="John" && peran===''){
    console.log("Halo John, Pilih peranmu untuk memulai game!")
}
 
//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
else if(nama==='Jane' && peran==='Penyihir'){
    console.log("Selamat datang di Dunia Werewolf, Jane")
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
}
 
//Output untuk Input nama = 'Jenita' dan peran 'Guard'
else if(nama==='Jenita' && peran==='Guard'){
    console.log("Selamat datang di Dunia Werewolf, Jenita")
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
}
 
//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
else if(nama='Junaedi' && peran==='Werewolf'){
    console.log("Selamat datang di Dunia Werewolf, Junaedi")
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
}



// Soal Switch Case

var hari = 21; 
var bulan = 1; 
var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 
var bulanText = '';

switch(bulan){
    case 1: bulanText='Januari'; break;
    case 2: bulanText='Februai'; break;
    case 3: bulanText='Maret'; break;
    case 4: bulanText='April'; break;
    case 5: bulanText='Mei'; break;
    case 6: bulanText='Juni'; break;
    case 7: bulanText='Juli'; break;
    case 8: bulanText='Agustus'; break;
    case 9: bulanText='September'; break;
    case 10: bulanText='Oktober'; break;
    case 11: bulanText='November'; break;
    case 12: bulanText='Desember'; break;
}

var showText = hari + ' ' + bulanText + ' ' + tahun

console.log("\r\nSoal Switch Case\r\n==============")
console.log(showText)